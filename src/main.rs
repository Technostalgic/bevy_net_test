mod core;

use bevy::prelude::*;

fn main() {
    App::new()
        .add_plugins((DefaultPlugins, core::CorePlugin))
        .run();
}
