use bevy::prelude::*;
use bevy_rapier2d::dynamics::Velocity;
use bevy_replicon::{
    prelude::*,
    renet::{transport::NetcodeClientTransport, ClientId, ServerEvent},
};
use serde::{Deserialize, Serialize};

use super::{
    instructions::{SyncPosition, SyncVelocity},
    networking::{is_client, is_server, NetState},
    session::{PlayerController, SessionState, TickCounter},
};

// Plugin: -----------------------------------------------------------------------------------------

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.replicate::<PlayerClient>()
            .replicate::<PlayerInputState>()
            .add_server_event::<ClientConnected>(EventType::Unordered)
            .add_client_event::<PlayerInputEvent>(EventType::Unreliable)
            .add_systems(
                PreUpdate,
                (
                    seek_current_player
                        .run_if(in_state(NetState::Connected))
                        .after(ClientSet::Receive),
                    handle_player_input
                        .run_if(in_state(SessionState::Playing))
                        .after(ClientSet::Receive)
                        .after(ServerSet::Receive),
                    handle_client_input_events
                        .run_if(is_server())
                        .run_if(in_state(SessionState::Playing))
                        .after(ServerSet::Receive),
                    sync_ticks_on_connect
                        .run_if(is_client())
                        .after(ClientSet::Receive),
                ),
            )
            .add_systems(
                PostUpdate,
                (send_client_connected_events, sync_heros)
                    .run_if(is_server())
                    .run_if(resource_exists::<TickCounter>())
                    .before(ServerSet::Send),
            );
    }
}

// Players: ----------------------------------------------------------------------------------------

#[derive(Component, Default, Clone, Copy, Reflect, Serialize, Deserialize)]
pub struct CurrentPlayer;

#[derive(Component, Debug, Clone, Serialize, Deserialize)]
pub struct PlayerClient(pub ClientId);

#[derive(Component, Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct PlayerInputState {
    pub last_tick_received: u32,
    pub movement: Vec2,
    pub is_jumping: bool,
}

#[derive(Bundle, Debug, Clone)]
pub struct PlayerBundle {
    pub client: PlayerClient,
    pub input_state: PlayerInputState,
    pub replication: Replication,
}

impl PlayerBundle {
    pub fn new(client_id: ClientId) -> Self {
        Self {
            client: PlayerClient(client_id),
            input_state: Default::default(),
            replication: Default::default(),
        }
    }
}

fn seek_current_player(
    mut commands: Commands,
    query: Query<(Entity, &PlayerClient), Added<PlayerClient>>,
    op_client: Option<Res<NetcodeClientTransport>>,
) {
    if query.is_empty() {
        return;
    }
    let cur_client_id = op_client.map_or(SERVER_ID.raw(), |c| c.client_id());
    println!("Current Player Client is {cur_client_id:?}, searching for current player");
    for (ent, plr_client) in &query {
        if plr_client.0.raw() == cur_client_id {
            commands.entity(ent).insert(CurrentPlayer);
            println!("found current player on {ent:?}");
        }
    }
}

fn handle_player_input(
    mut cur_player_query: Query<&mut PlayerInputState, With<CurrentPlayer>>,
    mut events: EventWriter<PlayerInputEvent>,
    keys_in: Res<Input<KeyCode>>,
    ticks: Res<TickCounter>,
) {
    let Ok(mut cur_player_input) = cur_player_query.get_single_mut() else {
        return;
    };
    cur_player_input.movement.x = 0.0;
    cur_player_input.is_jumping = false;
    if keys_in.pressed(KeyCode::A) {
        cur_player_input.movement.x -= 1.0;
    }
    if keys_in.pressed(KeyCode::D) {
        cur_player_input.movement.x += 1.0;
    }
    if keys_in.just_pressed(KeyCode::Space) {
        cur_player_input.is_jumping = true;
    }
    cur_player_input.last_tick_received = ticks.current_tick;
    events.send(PlayerInputEvent(
        ticks.current_tick,
        cur_player_input.clone(),
    ));
}

// Input Events: -----------------------------------------------------------------------------------

#[derive(Event, Debug, Deserialize, Serialize)]
pub struct PlayerInputEvent(pub u32, pub PlayerInputState);

fn handle_client_input_events(
    mut events: EventReader<FromClient<PlayerInputEvent>>,
    mut player_query: Query<(&PlayerClient, &mut PlayerInputState)>,
) {
    for evt in events.read() {
        for (client, mut plr_in) in &mut player_query {
            if evt.client_id == client.0 {
                if plr_in.last_tick_received < evt.event.1.last_tick_received {
                    *plr_in = evt.event.1.clone();
                    break;
                }
            }
        }
    }
}

// client is always resynced so this is no longer needed
fn _resync_client_on_input(
    mut commands: Commands,
    mut events: EventReader<FromClient<PlayerInputEvent>>,
    hero_query: Query<(Entity, &GlobalTransform, &Velocity, &PlayerController)>,
) {
    for evt in events.read() {
        for (ent, glob_trans, vel, plr_ctrl) in &hero_query {
            if plr_ctrl.0 == evt.client_id {
                commands.entity(ent).insert((
                    SyncPosition(
                        glob_trans.translation().truncate(),
                        glob_trans
                            .compute_transform()
                            .rotation
                            .to_euler(EulerRot::XYZ)
                            .2,
                    ),
                    SyncVelocity(vel.linvel, vel.angvel),
                ));
            }
        }
    }
}

// Client Connected Event: -------------------------------------------------------------------------

#[derive(Event, Debug, Serialize, Deserialize)]
pub struct ClientConnected {
    pub client: ClientId,
    pub tickstamp: u32,
}

fn sync_ticks_on_connect(
    mut commands: Commands,
    mut events: EventReader<ClientConnected>,
    client_trans: Res<NetcodeClientTransport>,
) {
    for evt in events.read() {
        if evt.client.raw() == client_trans.client_id() {
            println!("Connected to server at tick {:?}", evt.tickstamp);
            commands.insert_resource(TickCounter {
                current_tick: evt.tickstamp,
            });
        }
    }
}

fn send_client_connected_events(
    mut events: EventReader<ServerEvent>,
    mut event_send: EventWriter<ToClients<ClientConnected>>,
    tick_counter: Res<TickCounter>,
) {
    for evt in events.read() {
        match evt {
            ServerEvent::ClientConnected { client_id } => {
                println!(
                    "Sending client {:?} connected event at tick {:?}",
                    client_id, tick_counter.current_tick
                );
                event_send.send(ToClients {
                    event: ClientConnected {
                        client: client_id.clone(),
                        tickstamp: tick_counter.current_tick,
                    },
                    mode: SendMode::Broadcast,
                });
            }
            _ => {}
        }
    }
}

// ------------

fn sync_heros(
    mut hero_query: Query<
        (
            &GlobalTransform,
            &Velocity,
            &mut SyncPosition,
            &mut SyncVelocity,
        ),
        With<PlayerController>,
    >,
) {
    for (glob_trans, vel, mut sync_pos, mut sync_vel) in &mut hero_query {
        sync_pos.0 = glob_trans.translation().truncate();
        sync_pos.1 = glob_trans
            .compute_transform()
            .rotation
            .to_euler(EulerRot::XYZ)
            .2;
        *sync_vel = SyncVelocity(vel.linvel, vel.angvel);
    }
}
