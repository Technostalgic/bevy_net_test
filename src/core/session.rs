use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use bevy_replicon::{
    renet::{ClientId, ServerEvent},
    replicon_core::replication_rules::{AppReplicationExt, Replication},
    server::SERVER_ID,
};
use serde::{Deserialize, Serialize};

use crate::core::players::PlayerBundle;

use super::{
    instructions::{SyncedColliderBundle, SyncedPositionBundle, SyncedVelocityBundle},
    networking::{is_server, NetState},
    players::{PlayerClient, PlayerInputState},
};

// Plugin: -----------------------------------------------------------------------------------------

pub struct SessionPlugin;

impl Plugin for SessionPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            RapierPhysicsPlugin::<NoUserData>::default().in_fixed_schedule(),
            RapierDebugRenderPlugin::default(),
        ))
        .add_state::<SessionState>()
        .replicate::<SessionObject>()
        .replicate::<RigidBody>()
        .replicate::<PlayerController>()
        .configure_sets(FixedUpdate, SessionPreStep.before(PhysicsSet::SyncBackend))
        .add_systems(OnEnter(NetState::Connected), enter_gameplay_state)
        .add_systems(OnExit(NetState::Connected), leave_gameplay_state)
        .add_systems(
            OnEnter(SessionState::Playing),
            (setup_any, setup_server_session.run_if(is_server())),
        )
        .add_systems(OnExit(SessionState::Playing), destruct)
        .add_systems(
            FixedUpdate,
            (
                handle_tick_increment.in_set(SessionPreStep),
                handle_body_movement.after(SessionPreStep),
            )
                .run_if(resource_exists::<TickCounter>()),
        )
        .add_systems(Update, handle_server_events.run_if(is_server()));
    }
}

// Session States: ---------------------------------------------------------------------------------

#[derive(States, Debug, Default, Clone, Hash, PartialEq, Eq)]
pub enum SessionState {
    #[default]
    None,
    Playing,
}

fn enter_gameplay_state(mut next_state: ResMut<NextState<SessionState>>) {
    next_state.0 = Some(SessionState::Playing);
}

fn leave_gameplay_state(mut next_state: ResMut<NextState<SessionState>>) {
    next_state.0 = Some(SessionState::None);
}

// Tick Counter: -----------------------------------------------------------------------------------

#[derive(
    SystemSet, Default, Debug, Reflect, Clone, Hash, PartialEq, Eq, Serialize, Deserialize,
)]
pub struct SessionPreStep;

#[derive(Resource, Default, Debug, Reflect, Clone, Serialize, Deserialize)]
pub struct TickCounter {
    pub current_tick: u32,
}

fn handle_tick_increment(mut counter: ResMut<TickCounter>) {
    counter.current_tick += 1;
}

// Setup: ------------------------------------------------------------------------------------------

#[derive(Component, Default, Debug, Reflect, Clone, Serialize, Deserialize)]
pub struct SessionObject;

fn setup_any(
    mut phys_debug_render: ResMut<DebugRenderContext>,
    mut phys_config: ResMut<RapierConfiguration>,
) {
    phys_debug_render.enabled = true;
    phys_config.gravity = Vec2::Y * -1000.0;
}

fn setup_server_session(mut commands: Commands) {
    commands.insert_resource(TickCounter::default());
    // spawn floor
    commands.spawn((
        SessionObject,
        Replication,
        SyncedPositionBundle::new(Vec3::new(0.0, -330.0, 0.0)),
        SyncedColliderBundle::new(Collider::cuboid(500.0, 25.0)),
    ));
    // spawn walls
    commands.spawn((
        SessionObject,
        Replication,
        SyncedPositionBundle::new(Vec3::new(-500.0, -115.0, 0.0)),
        SyncedColliderBundle::new(Collider::cuboid(25.0, 200.0)),
    ));
    commands.spawn((
        SessionObject,
        Replication,
        SyncedPositionBundle::new(Vec3::new(500.0, -115.0, 0.0)),
        SyncedColliderBundle::new(Collider::cuboid(25.0, 200.0)),
    ));
    // spawn controllable rigidbody
    commands.spawn(PlayerBodyBundle::new(SERVER_ID, Vec2::ZERO));
}

fn destruct(mut commands: Commands, query: Query<Entity, With<SessionObject>>) {
    commands.remove_resource::<TickCounter>();
    for ent in &query {
        commands.entity(ent).despawn();
    }
}

// Connection Events: ------------------------------------------------------------------------------

fn handle_server_events(mut commands: Commands, mut events: EventReader<ServerEvent>) {
    for event in events.read() {
        match event {
            ServerEvent::ClientConnected { client_id } => {
                println!("Client connected: {client_id}");
                commands.spawn(PlayerBundle::new(*client_id));
                commands.spawn(PlayerBodyBundle::new(*client_id, Vec2::ZERO));
            }
            ServerEvent::ClientDisconnected { client_id, reason } => {
                println!("Disconnected {client_id}: {reason} ");
            }
        }
    }
}

// Player Controller: ------------------------------------------------------------------------------

#[derive(Component, Debug, Clone, Serialize, Deserialize)]
pub struct PlayerController(pub ClientId);

#[derive(Bundle, Debug, Clone)]
pub struct PlayerBodyBundle {
    pub session_obj: SessionObject,
    pub replication: Replication,
    pub player_controller: PlayerController,
    pub sync_pos: SyncedPositionBundle,
    pub sync_col: SyncedColliderBundle,
    pub sync_vel: SyncedVelocityBundle,
    pub rigid_body: RigidBody,
}

impl PlayerBodyBundle {
    pub fn new(client_id: ClientId, position: Vec2) -> Self {
        Self {
            session_obj: SessionObject,
            replication: Replication,
            player_controller: PlayerController(client_id),
            sync_pos: SyncedPositionBundle::new(position.extend(0.0)),
            sync_col: SyncedColliderBundle::new(Collider::cuboid(50.0, 50.0)),
            sync_vel: SyncedVelocityBundle::new(Vec2::ZERO, 0.0),
            rigid_body: RigidBody::Dynamic,
        }
    }
}

pub fn handle_body_movement(
    time: Res<Time<Fixed>>,
    player_query: Query<(&PlayerClient, &PlayerInputState)>,
    mut body_query: Query<(&PlayerController, &mut Velocity)>,
) {
    const PLAYER_ACCELERATION: f32 = 2500.0;
    const PLAYER_MAX_SPEED: f32 = 500.0;
    const PLAYER_JUMP_VEL: f32 = 500.0;
    let dt = time.delta_seconds();
    for (plr_client, plr_input) in &player_query {
        for (plr_controller, mut plr_vel) in &mut body_query {
            if plr_controller.0 == plr_client.0 {
                let fvel = plr_vel.linvel.x + plr_input.movement.x * dt * PLAYER_ACCELERATION;
                plr_vel.linvel.x = fvel.clamp(-PLAYER_MAX_SPEED, PLAYER_MAX_SPEED);
                if plr_input.is_jumping {
                    plr_vel.linvel.y =  PLAYER_JUMP_VEL;
                }
                break;
            }
        }
    }
}
