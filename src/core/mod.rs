mod instructions;
mod networking;
mod players;
mod session;
mod ui;

use bevy::prelude::*;

use self::networking::{NetState, NetworkingSystems};

// Plugin: -----------------------------------------------------------------------------------------

pub struct CorePlugin;

impl Plugin for CorePlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            networking::NetworkingPlugin,
            players::PlayerPlugin,
            instructions::InstructionPlugin,
            session::SessionPlugin,
            ui::UiPlugin,
        ))
        .add_systems(Startup, setup)
        .add_systems(
            Update,
            (
                handle_network_select.run_if(in_state(NetState::NetworkSelect)),
                handle_escape,
            ),
        );
    }
}

fn setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

// Handle Network Selection: -----------------------------------------------------------------------

fn handle_network_select(
    mut commands: Commands,
    key_in: Res<Input<KeyCode>>,
    net_sys: Res<NetworkingSystems>,
) {
    for key in key_in.get_just_pressed() {
        match key {
            KeyCode::Key1 => commands.run_system(net_sys.create_server),
            KeyCode::Key2 => commands.run_system(net_sys.connect_client),
            _ => {}
        }
    }
}

fn handle_escape(
    mut commands: Commands,
    net_sys: Res<NetworkingSystems>,
    key_in: Res<Input<KeyCode>>,
    mut next_state: ResMut<NextState<NetState>>,
) {
    if key_in.just_pressed(KeyCode::Escape) {
        next_state.0 = Some(NetState::NetworkSelect);
        commands.run_system(net_sys.disconnect);
    }
}
