use std::{
    net::{Ipv4Addr, SocketAddr, UdpSocket},
    time::SystemTime,
};

use bevy::{ecs::system::SystemId, prelude::*};
use bevy_replicon::{
    renet::{
        transport::{
            ClientAuthentication, NetcodeClientTransport, NetcodeServerTransport,
            ServerAuthentication, ServerConfig,
        },
        ConnectionConfig, RenetClient, RenetServer,
    },
    replicon_core::NetworkChannels,
    server::SERVER_ID,
    *,
};

use crate::core::players::{CurrentPlayer, PlayerBundle};

// Plugin: -----------------------------------------------------------------------------------------

pub struct NetworkingPlugin;

impl Plugin for NetworkingPlugin {
    fn build(&self, app: &mut App) {
        let sys_ids = NetworkingSystems {
            create_server: app.world.register_system(create_server),
            connect_client: app.world.register_system(connect_client),
            disconnect: app.world.register_system(disconnect),
        };
        app.add_plugins(ReplicationPlugins)
            .add_state::<NetState>()
            .insert_resource(sys_ids)
            .add_systems(
                OnExit(NetState::Connected),
                (
                    on_leave_connected_state.run_if(is_client()),
                    on_server_leave_connected_state.run_if(is_server()),
                ),
            )
            .add_systems(
                Update,
                (
                    handle_connecting.run_if(in_state(NetState::Connecting)),
                    handle_connected
                        .run_if(in_state(NetState::Connected))
                        .run_if(is_client()),
                ),
            );
    }
}

// Net State: --------------------------------------------------------------------------------------

#[derive(States, Debug, Default, Clone, Hash, PartialEq, Eq)]
pub enum NetState {
    #[default]
    NetworkSelect,
    Connecting,
    Connected,
}

// Server: -----------------------------------------------------------------------------------------

fn on_server_leave_connected_state(mut commands: Commands) {
    commands.remove_resource::<RenetServer>();
    commands.remove_resource::<NetcodeServerTransport>();
}

// Client: -----------------------------------------------------------------------------------------

fn handle_connecting(client: Res<RenetClient>, mut next_state: ResMut<NextState<NetState>>) {
    if client.is_connected() {
        next_state.0 = Some(NetState::Connected);
    }
}

fn on_leave_connected_state(mut commands: Commands) {
    commands.remove_resource::<RenetClient>();
    commands.remove_resource::<NetcodeClientTransport>();
}

fn handle_connected(client: Res<RenetClient>, mut next_state: ResMut<NextState<NetState>>) {
    if client.is_disconnected() {
        next_state.0 = Some(NetState::NetworkSelect);
    }
}

// Oneshot Systems: --------------------------------------------------------------------------------

const PORT: u16 = 5577;
const PROTOCOL_ID: u64 = 0;

#[derive(Resource, Debug, Clone)]
pub struct NetworkingSystems {
    /// create and host server instance
    pub create_server: SystemId,
    /// create client and connect to a server
    pub connect_client: SystemId,
    /// disconnect from the server if client; if server, close the server
    pub disconnect: SystemId,
}

fn create_server(
    mut commands: Commands,
    network_channels: Res<NetworkChannels>,
    game_state: Res<State<NetState>>,
    mut next_state: ResMut<NextState<NetState>>,
) {
    if *game_state != NetState::NetworkSelect {
        return;
    }
    println!("Creating Server...");

    // change the gamestate away from selection
    next_state.0 = Some(NetState::Connected);

    // create server
    let server = RenetServer::new(ConnectionConfig {
        client_channels_config: network_channels.get_client_configs(),
        server_channels_config: network_channels.get_server_configs(),
        ..Default::default()
    });
    commands.insert_resource(server);

    // create server config/netcode transport.. whatever that means
    let Ok(current_time) = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) else {
        panic!("can't calculate time for some reason");
    };
    let pub_addr = SocketAddr::new(Ipv4Addr::LOCALHOST.into(), PORT);
    let Ok(socket) = UdpSocket::bind(pub_addr) else {
        panic!("can't bind address '{:?}' to UDP socket", pub_addr);
    };
    let server_config = ServerConfig {
        current_time,
        max_clients: 8,
        protocol_id: PROTOCOL_ID,
        authentication: ServerAuthentication::Unsecure,
        public_addresses: [pub_addr].to_vec(),
    };
    let Ok(transport) = NetcodeServerTransport::new(server_config, socket) else {
        panic!("can't establish netcode server transport");
    };
    commands.insert_resource(transport);

    // create a client for the player hosting the server
    commands.spawn((CurrentPlayer, PlayerBundle::new(SERVER_ID)));

    println!("Server Created!")
}

fn connect_client(
    mut commands: Commands,
    network_channels: Res<NetworkChannels>,
    game_state: Res<State<NetState>>,
    mut next_state: ResMut<NextState<NetState>>,
) {
    if *game_state != NetState::NetworkSelect {
        return;
    }

    // create renet client for what I assume is info that the networking crate needs
    let client = RenetClient::new(ConnectionConfig {
        server_channels_config: network_channels.get_server_configs(),
        client_channels_config: network_channels.get_client_configs(),
        ..Default::default()
    });
    commands.insert_resource(client);

    // create server config/netcode transport.. whatever that means
    let Ok(current_time) = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) else {
        panic!("can't calculate time for some reason");
    };
    let client_id = current_time.as_millis() as u64;
    let server_addr = SocketAddr::new(Ipv4Addr::LOCALHOST.into(), PORT);
    let Ok(socket) = UdpSocket::bind((server_addr.ip(), 0)) else {
        panic!("can't bind address '{:?}' to UDP socket", server_addr);
    };
    let authentication = ClientAuthentication::Unsecure {
        client_id,
        protocol_id: PROTOCOL_ID,
        server_addr,
        user_data: None,
    };
    let Ok(transport) = NetcodeClientTransport::new(current_time, authentication, socket) else {
        panic!("can't establish netcode server transport");
    };
    commands.insert_resource(transport);

    println!("Connecting Client...");
    next_state.0 = Some(NetState::Connecting);
}

fn disconnect(
    op_server: Option<ResMut<RenetServer>>,
    op_client: Option<ResMut<RenetClient>>,
    mut next_state: ResMut<NextState<NetState>>,
) {
    if let Some(mut server) = op_server {
        server.disconnect_all();
        next_state.0 = Some(NetState::NetworkSelect);
    }
    if let Some(mut client) = op_client {
        client.disconnect();
        next_state.0 = Some(NetState::NetworkSelect);
    }
}

// Conditions: -------------------------------------------------------------------------------------

pub fn is_server() -> impl FnMut(Option<Res<RenetServer>>) -> bool + Clone {
    resource_exists::<RenetServer>()
}

pub fn is_client() -> impl FnMut(Option<Res<RenetClient>>) -> bool + Clone {
    resource_exists::<RenetClient>()
}
