use bevy::prelude::*;
use bevy_replicon::renet::{RenetClient, RenetServer};

use super::NetState;

// Plugin: -----------------------------------------------------------------------------------------

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(NetState::NetworkSelect), create_network_select_text)
            .add_systems(OnExit(NetState::NetworkSelect), remove_network_select_text)
            .add_systems(OnEnter(NetState::Connecting), create_connecting_text)
            .add_systems(OnExit(NetState::Connecting), remove_connecting_text)
            .add_systems(OnEnter(NetState::Connected), create_gameplay_text)
            .add_systems(OnExit(NetState::Connected), remove_gameplay_text)
            .add_systems(
                Update,
                handle_server_info_text
                    .run_if(in_state(NetState::Connected))
                    .run_if(resource_exists::<RenetServer>()),
            );
    }
}

// Network Select Text: ----------------------------------------------------------------------------

#[derive(Component, Default, Clone, Reflect)]
struct NetworkSelectText;

fn create_network_select_text(mut commands: Commands) {
    commands.spawn((
        NetworkSelectText,
        TextBundle::from_section(
            "1: Server \n2: Client",
            TextStyle {
                font_size: 50.0,
                ..Default::default()
            },
        ),
    ));
}

fn remove_network_select_text(
    mut commands: Commands,
    query: Query<Entity, With<NetworkSelectText>>,
) {
    for ent in &query {
        commands.entity(ent).despawn();
    }
}

// Connecting Text: --------------------------------------------------------------------------------

#[derive(Component, Default, Clone, Reflect)]
struct NetworkConnectingText;

fn create_connecting_text(mut commands: Commands) {
    commands.spawn((
        NetworkConnectingText,
        TextBundle::from_section(
            "Connecting...",
            TextStyle {
                font_size: 40.0,
                color: Color::YELLOW,
                ..Default::default()
            },
        ),
    ));
}

fn remove_connecting_text(
    mut commands: Commands,
    query: Query<Entity, With<NetworkConnectingText>>,
) {
    for ent in &query {
        commands.entity(ent).despawn();
    }
}

// Gameplay Text: ----------------------------------------------------------------------------------

#[derive(Component, Default, Clone, Reflect)]
struct NetworkGameplayText;

#[derive(Component, Default, Clone, Reflect)]
struct ServerInfoText;

fn create_gameplay_text(
    mut commands: Commands,
    op_server: Option<Res<RenetServer>>,
    op_client: Option<Res<RenetClient>>,
) {
    let text = if op_server.is_some() {
        "Hosting"
    } else if op_client.is_some() {
        "Connected"
    } else {
        "Error"
    };
    let color = if op_server.is_some() || op_client.is_some() {
        Color::GREEN
    } else {
        Color::RED
    };
    // spawn server/client state text
    commands.spawn((
        NetworkGameplayText,
        TextBundle::from_section(
            text,
            TextStyle {
                font_size: 20.0,
                color,
                ..Default::default()
            },
        ),
    ));
    // spawn server info text
    if op_server.is_some() {
        commands.spawn((
            ServerInfoText,
            TextBundle::from_sections([
                TextSection {
                    value: "Connected Clients: ".to_string(),
                    style: TextStyle {
                        font_size: 15.0,
                        ..Default::default()
                    },
                    ..Default::default()
                },
                TextSection {
                    value: "0".to_string(),
                    style: TextStyle {
                        font_size: 15.0,
                        ..Default::default()
                    },
                    ..Default::default()
                },
            ])
            .with_style(Style {
                top: Val::Px(50.0),
                ..Default::default()
            }),
        ));
    }
}

fn handle_server_info_text(
    mut query: Query<&mut Text, With<ServerInfoText>>,
    server: Res<RenetServer>,
) {
    for mut text in &mut query {
        text.sections[1].value = format!("{}", server.connected_clients());
    }
}

fn remove_gameplay_text(
    mut commands: Commands,
    query: Query<Entity, Or<(With<NetworkGameplayText>, With<ServerInfoText>)>>,
) {
    for ent in &query {
        commands.entity(ent).despawn();
    }
}
