use bevy::prelude::*;
use bevy_rapier2d::prelude::*;
use bevy_replicon::{prelude::*, renet::ServerEvent};
use serde::{Deserialize, Serialize};

use super::{
    networking::{is_client, is_server},
    session::SessionObject,
};

// Plugin: -----------------------------------------------------------------------------------------

pub struct InstructionPlugin;

impl Plugin for InstructionPlugin {
    fn build(&self, app: &mut App) {
        app.configure_sets(
            PreUpdate,
            ClientNetInstructions
                .run_if(is_client())
                .after(ClientSet::Receive),
        )
        .replicate::<SyncPosition>()
        .replicate::<SyncVelocity>()
        .replicate::<SyncCollider>()
        .add_systems(
            PreUpdate,
            (sync_positions, sync_colliders, sync_velocities).in_set(ClientNetInstructions),
        )
        .add_systems(
            PostUpdate,
            handle_client_catchup
                .run_if(is_server())
                .before(ServerSet::Send),
        );
    }
}

// System Sets: ------------------------------------------------------------------------------------

#[derive(SystemSet, Debug, Reflect, PartialEq, Eq, Hash, Clone)]
pub struct ClientNetInstructions;

// Sync Position Instruction: ----------------------------------------------------------------------

#[derive(Component, Default, Clone, Debug, Reflect, Serialize, Deserialize)]
pub struct SyncPosition(pub Vec2, pub f32);

fn sync_positions(
    mut commands: Commands,
    mut query: Query<(Entity, Option<&mut Transform>, &SyncPosition)>,
) {
    for (ent, op_trans, pos) in &mut query {
        sync_position(&mut commands, ent, pos, op_trans);
    }
}

fn sync_position(
    commands: &mut Commands,
    ent: Entity,
    sync_pos: &SyncPosition,
    op_trans: Option<Mut<Transform>>,
) {
    if let Some(mut trans) = op_trans {
        trans.translation.x = sync_pos.0.x;
        trans.translation.y = sync_pos.0.y;
        trans.rotation = Quat::from_euler(EulerRot::XYZ, 0.0, 0.0, sync_pos.1);
    } else {
        commands.entity(ent).insert(TransformBundle::from_transform(
            Transform::from_translation(sync_pos.0.extend(0.0)).with_rotation(Quat::from_euler(
                EulerRot::XYZ,
                0.0,
                0.0,
                sync_pos.1,
            )),
        ));
    }
    commands.entity(ent).remove::<SyncPosition>();
}

#[derive(Bundle, Default, Clone, Debug)]
pub struct SyncedPositionBundle {
    sync_pos: SyncPosition,
    transforms: TransformBundle,
}

impl SyncedPositionBundle {
    pub fn new(world_pos: Vec3) -> Self {
        Self {
            sync_pos: SyncPosition(world_pos.truncate(), 0.0),
            transforms: TransformBundle::from_transform(Transform::from_translation(world_pos)),
        }
    }
}

// Sync Velocity Instruction: ----------------------------------------------------------------------

#[derive(Component, Default, Clone, Debug, Reflect, Serialize, Deserialize)]
pub struct SyncVelocity(pub Vec2, pub f32);

fn sync_velocities(
    mut commands: Commands,
    mut query: Query<(Entity, Option<&mut Velocity>, &SyncVelocity)>,
) {
    for (ent, op_vel, sync_vel) in &mut query {
        sync_velocity(&mut commands, ent, sync_vel, op_vel);
    }
}

fn sync_velocity(
    commands: &mut Commands,
    ent: Entity,
    sync_vel: &SyncVelocity,
    op_vel: Option<Mut<Velocity>>,
) {
    if let Some(mut vel) = op_vel {
        vel.linvel = sync_vel.0;
        vel.angvel = sync_vel.1;
    } else {
        commands.entity(ent).insert(Velocity {
            linvel: sync_vel.0,
            angvel: sync_vel.1,
        });
    }
    commands.entity(ent).remove::<SyncVelocity>();
}

#[derive(Bundle, Default, Clone, Debug, Reflect, Serialize, Deserialize)]
pub struct SyncedVelocityBundle {
    sync_vel: SyncVelocity,
    vel: Velocity,
}

impl SyncedVelocityBundle {
    pub fn new(velocity: Vec2, angular_vel: f32) -> Self {
        Self {
            sync_vel: SyncVelocity(velocity, angular_vel),
            vel: Velocity {
                linvel: velocity,
                angvel: angular_vel,
            },
        }
    }
}

// Sync Collider Instruction: ----------------------------------------------------------------------

#[derive(Component, Default, Clone, Debug, Serialize, Deserialize)]
pub struct SyncCollider(pub Option<Collider>);

fn sync_colliders(mut commands: Commands, mut query: Query<(Entity, &mut SyncCollider)>) {
    for (ent, mut sync_col) in &mut query {
        if let Some(col) = sync_col.0.take() {
            commands.entity(ent).insert(col);
            commands.entity(ent).remove::<SyncCollider>();
        }
    }
}

#[derive(Bundle, Default, Clone, Debug)]
pub struct SyncedColliderBundle {
    sync_collider: SyncCollider,
    collider: Collider,
}

impl SyncedColliderBundle {
    pub fn new(collider: Collider) -> Self {
        Self {
            sync_collider: SyncCollider(Some(collider.clone())),
            collider,
        }
    }
}

// Update Instructructions on Client Connect: ------------------------------------------------------

type InstructionQuery<'world, 'state, 'a, 'b, 'c, 'd> = Query<
    'world,
    'state,
    (
        Option<&'a mut SyncPosition>,
        Option<&'b Transform>,
        Option<&'c mut SyncVelocity>,
        Option<&'d Velocity>,
    ),
    With<SessionObject>,
>;

fn handle_client_catchup(mut events: EventReader<ServerEvent>, mut query: InstructionQuery) {
    for evt in events.read() {
        match evt {
            ServerEvent::ClientConnected { client_id: _ } => {
                update_all_instructions(&mut query);
            }
            _ => {}
        }
    }
}

fn update_all_instructions(query: &mut InstructionQuery) {
    for (op_sync_pos, op_trans, op_sync_vel, op_vel) in query {
        if let Some(mut sync_pos) = op_sync_pos {
            if let Some(pos) = op_trans {
                sync_pos.0.x = pos.translation.x;
                sync_pos.0.y = pos.translation.y;
            }
        }
        if let Some(mut sync_vel) = op_sync_vel {
            if let Some(vel) = op_vel {
                sync_vel.0.x = vel.linvel.x;
                sync_vel.0.y = vel.linvel.y;
                sync_vel.1 = vel.angvel;
            }
        }
    }
}
